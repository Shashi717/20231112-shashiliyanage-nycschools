
#  NYC High Schools


## Areas of Emphasis

- Implemented MVVM architecture for cleaner and modularizable codebase.
- Integrated Unit Tests for cache functionality and data decoding.
- Incorporated error handling for network requests and data decoding.

## Notes
- Some schools lack SAT score information.

## Future Focus with Additional Time:

- Enhance UI by displaying more information (e.g., school website links)
- Refine UI elements.
- Expand Unit Test coverage for view models and related components.
