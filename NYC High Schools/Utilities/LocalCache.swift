//
//  LocalCache.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/11/23.
//

import Foundation

class LocalCache<T: AnyObject> {
  private let cache = NSCache<NSString,T>()

  init(limit: Int) {
    cache.countLimit = limit
  }

  func addItem(_ item: T, for key: String) {
    cache.setObject(item, forKey: NSString(string: key))
  }

  func getItem(_ key: String) -> T? {
    return cache.object(forKey: NSString(string: key))
  }
}
