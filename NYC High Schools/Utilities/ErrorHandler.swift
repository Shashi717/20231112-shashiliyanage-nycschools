//
//  ErrorHandler.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/11/23.
//

import UIKit

enum ErrorHandler {
  static func displayAlert(title: String, error: Error, presentFrom viewController: UIViewController) {
    let alertViewController = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
    let alertAction = UIAlertAction(title: "OK", style: .default)
    alertViewController.addAction(alertAction)
    viewController.present(alertViewController, animated: true)
  }
}
