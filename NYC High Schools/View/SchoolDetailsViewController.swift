//
//  SchoolDetailsViewController.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import UIKit

class SchoolDetailsViewController: UIViewController {

  private let schoolViewModel: SchoolViewModel
  private let satScoresViewModel: SATScoresViewModel

  private static let padding: CGFloat = 32.0

  init(schoolViewModel: SchoolViewModel, satScoresViewModel: SATScoresViewModel) {
    self.schoolViewModel = schoolViewModel
    self.satScoresViewModel = satScoresViewModel

    super.init(nibName: nil, bundle: .main)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .white

    satScoresViewModel.delegate = self

    // load the SAT scores if it's not already loaded from cache
    if !satScoresViewModel.isSATScoresLoaded {
      Task {
        await satScoresViewModel.loadSATScores()
      }
    }
  }

  override func loadView() {
    super.loadView()

    satScoresStackView.translatesAutoresizingMaskIntoConstraints = false

    satScoresTitleLabel.translatesAutoresizingMaskIntoConstraints = false
    satTestTakersCountLabel.translatesAutoresizingMaskIntoConstraints = false
    satCriticalReadingAvgScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    satMathAvgScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    satWritingAvgScoreLabel.translatesAutoresizingMaskIntoConstraints = false

    schoolNameLabel.text = schoolViewModel.name
    schoolDescriptionLabel.text = schoolViewModel.overview

    satScoresTitleLabel.font = .boldSystemFont(ofSize: 16.0)

    view.addSubview(schoolNameLabel)
    view.addSubview(schoolDescriptionLabel)
    view.addSubview(satScoresStackView)

    setupUI()
    updateUI()
  }

  private func setupUI() {
    let safeAreaLayoutGuide = view.safeAreaLayoutGuide
    NSLayoutConstraint.activate([
      schoolNameLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: Self.padding),
      schoolNameLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: Self.padding),
      schoolNameLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -Self.padding),
      schoolDescriptionLabel.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: Self.padding),
      schoolDescriptionLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: Self.padding),
      schoolDescriptionLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -Self.padding),
      satScoresStackView.topAnchor.constraint(equalTo: schoolDescriptionLabel.bottomAnchor, constant: Self.padding),
      satScoresStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: Self.padding),
      satScoresStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -Self.padding)
    ])
  }

  private func updateUI() {
    // update the labels with scores if the data is present
    guard let numOfTestTakers = satScoresViewModel.numOfTestTakers,
          let criticalReadingAvgScore = satScoresViewModel.criticalReadingAvgScore,
          let mathAvgScore = satScoresViewModel.mathAvgScore,
          let writingAvgScore = satScoresViewModel.writingAvgScore else {
      return
    }

    satScoresTitleLabel.text = "SAT Scores"
    satTestTakersCountLabel.text = "Number of test takers: \(numOfTestTakers)"
    satCriticalReadingAvgScoreLabel.text = "Critical Reading Average Score: \(criticalReadingAvgScore)"
    satMathAvgScoreLabel.text = "Math Average Score: \(mathAvgScore)"
    satWritingAvgScoreLabel.text = "Writing Average Score: \(writingAvgScore)"
  }

  private lazy var schoolNameLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.font = .boldSystemFont(ofSize: 25.0)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  private lazy var schoolDescriptionLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .left
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  private var satScoresTitleLabel = UILabel()
  private var satTestTakersCountLabel = UILabel()
  private var satCriticalReadingAvgScoreLabel = UILabel()
  private var satMathAvgScoreLabel = UILabel()
  private var satWritingAvgScoreLabel = UILabel()
  private lazy var satScoresStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.alignment = .center
    stackView.spacing = 8.0
    stackView.addArrangedSubview(satScoresTitleLabel)
    stackView.addArrangedSubview(satTestTakersCountLabel)
    stackView.addArrangedSubview(satCriticalReadingAvgScoreLabel)
    stackView.addArrangedSubview(satMathAvgScoreLabel)
    stackView.addArrangedSubview(satWritingAvgScoreLabel)
    return stackView
  }()
}

extension SchoolDetailsViewController: SATScoresViewModelDelegate {
  func didUpdate(_ error: Error?) {
    DispatchQueue.main.async { [weak self] in
      if let error = error,
         let self = self {
        // show an alert if there's an error
        ErrorHandler.displayAlert(title: "Error fetching SAT scores", error: error, presentFrom: self)
      } else {
        self?.updateUI()
      }
    }
  }
}
