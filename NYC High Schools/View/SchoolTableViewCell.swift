//
//  SchoolTableViewCell.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

  static var reuserIdentifier: String = "SchoolCell"
  private static var padding: CGFloat = 16.0

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none

    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    addressLabel.translatesAutoresizingMaskIntoConstraints = false
    phoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false

    contentView.addSubview(nameLabel)
    contentView.addSubview(addressLabel)
    contentView.addSubview(phoneNumberLabel)

    setupUI()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupUI() {
    NSLayoutConstraint.activate([
      nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Self.padding),
      nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Self.padding),
      nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Self.padding),
      addressLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: Self.padding),
      addressLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Self.padding),
      addressLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Self.padding),
      phoneNumberLabel.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: Self.padding),
      phoneNumberLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Self.padding),
      phoneNumberLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Self.padding),
      phoneNumberLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Self.padding),
    ])
  }

  func configure(_ schoolViewModel: SchoolViewModel) {
    nameLabel.text = schoolViewModel.name
    addressLabel.text = schoolViewModel.location
    phoneNumberLabel.text = schoolViewModel.phoneNumber
  }

  private lazy var nameLabel: UILabel = {
    let label = UILabel()
    label.font = .boldSystemFont(ofSize: 18.0)
    label.numberOfLines = 0
    return label
  }()

  private lazy var addressLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    return label
  }()

  private var phoneNumberLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 1
    return label
  }()
}
