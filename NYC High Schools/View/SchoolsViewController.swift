//
//  SchoolsViewController.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import UIKit

class SchoolsViewController: UIViewController {

  private let schoolsViewModel: SchoolsViewModel
  private let apiClient: APIClient
  private let cache: LocalCache<SATScoresViewModel>

  private let schoolsTableView = UITableView()

  private static let padding: CGFloat = 16.0

  init(schoolsViewModel: SchoolsViewModel, apiClient: APIClient, cache: LocalCache<SATScoresViewModel>) {
    self.schoolsViewModel = schoolsViewModel
    self.apiClient = apiClient
    self.cache = cache
    super.init(nibName: nil, bundle: .main)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .white
    title = "NYC High Schools"

    schoolsTableView.register(SchoolTableViewCell.self, forCellReuseIdentifier: SchoolTableViewCell.reuserIdentifier)
    schoolsTableView.rowHeight = UITableView.automaticDimension
    schoolsTableView.delegate = self
    schoolsTableView.dataSource = self
    schoolsViewModel.delegate = self

    Task {
      await schoolsViewModel.loadSchools()
    }
  }

  override func loadView() {
    super.loadView()

    schoolsTableView.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(schoolsTableView)

    setupUI()
  }

  private func setupUI() {
    let safeAreaLayoutGuide = view.safeAreaLayoutGuide
    NSLayoutConstraint.activate([
      schoolsTableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: Self.padding),
      schoolsTableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: Self.padding),
      schoolsTableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -Self.padding),
      schoolsTableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -Self.padding)
    ])
  }
}

extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return schoolsViewModel.schools?.count ?? 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.reuserIdentifier, for: indexPath) as? SchoolTableViewCell,
       let schoolViewModel = schoolsViewModel.schools?[indexPath.row] {
      // configure cell with view model
      cell.configure(schoolViewModel)
      return cell
    }
    // assert if failure
    // this line should not be evaluated
    assertionFailure("Failed to deque \(SchoolTableViewCell.reuserIdentifier) at index \(indexPath.row)")
    return UITableViewCell()
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let schoolViewModel = schoolsViewModel.schools?[indexPath.row] else {
      return
    }
    let satScoresViewModel: SATScoresViewModel
    // check if the SAT scores are already in the cache
    if let viewModel = cache.getItem(schoolViewModel.school.dbn) {
      satScoresViewModel = viewModel
    } else {
      // if not create a new view model
      satScoresViewModel = SATScoresViewModel(dbn: schoolViewModel.school.dbn, apiClient: apiClient,  cache: cache)
    }
    let schoolDetailsViewController = SchoolDetailsViewController(schoolViewModel: schoolViewModel, satScoresViewModel: satScoresViewModel)
    self.navigationController?.pushViewController(schoolDetailsViewController, animated: true)
  }
}

extension SchoolsViewController: SchoolsViewModelDelegate {
  func didUpdate(_ error: Error?) {
    DispatchQueue.main.async { [weak self] in
      if let error = error,
         let self = self {
        // show an alert if there's an error
        ErrorHandler.displayAlert(title: "Error fetching schools", error: error, presentFrom: self)
      } else {
        self?.schoolsTableView.reloadData()
      }
    }
  }
}
