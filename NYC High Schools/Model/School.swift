//
//  School.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

struct School: Codable {

  var dbn: String
  var schoolName: String
  var boro: String
  var overviewParagraph: String
  var location: String
  var phoneNumber: String
  var website: String

  enum CodingKeys: String, CodingKey {
    case dbn
    case schoolName = "school_name"
    case boro
    case overviewParagraph = "overview_paragraph"
    case location
    case phoneNumber = "phone_number"
    case website
  }
}
