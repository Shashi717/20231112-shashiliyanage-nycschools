//
//  SATScores.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

struct SATScores: Codable {
  var dbn: String
  var schoolName: String
  var numOfSATTestTakers: String
  var satCriticalReadingAvgScore: String
  var satMathAvgScore: String
  var satWritingAvgScore: String

  enum CodingKeys: String, CodingKey {
    case dbn
    case schoolName = "school_name"
    case numOfSATTestTakers = "num_of_sat_test_takers"
    case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
    case satMathAvgScore = "sat_math_avg_score"
    case satWritingAvgScore = "sat_writing_avg_score"
  }
}
