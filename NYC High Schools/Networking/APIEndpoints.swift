//
//  APIEndpoints.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import Foundation

struct APIEndpoints {
  private static let schoolsAPIEndpoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
  private static let SATScoresAPIEndpoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

  func getSchoolsAPIEndpoint() -> URL? {
    return URL(string: APIEndpoints.schoolsAPIEndpoint)
  }

  func getSATScoresAPIEndpoint(for dbn: String) -> URL? {
    var components = URLComponents(string: APIEndpoints.SATScoresAPIEndpoint)
    components?.queryItems = [URLQueryItem(name: "dbn", value: dbn)]
    return components?.url
  }
}
