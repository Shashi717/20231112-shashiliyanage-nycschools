//
//  APIClient.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import Foundation

struct APIClient {
  let apiService: APIServiceProtocol

  func fetchSchools(_ url: URL) async throws -> [School] {
    let data = try await apiService.fetchData(url)
    guard let schools = try? JSONDecoder().decode([School].self, from: data) else {
      throw APIError.decodingError
    }
    return schools
  }

  func fetchSATScores(_ url: URL) async throws -> SATScores? {
    // some schools don't have SAT score data so we're returning
    // a nullable SATScore
    let data = try await apiService.fetchData(url)
    guard let scoreArray = try? JSONDecoder().decode([SATScores].self, from: data) else {
      throw APIError.decodingError
    }
    // SAT scores are fetched as an array that has one element so return
    // the first element
    return scoreArray.first
  }
}
