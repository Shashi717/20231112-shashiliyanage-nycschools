//
//  APIService.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import Foundation

protocol APIServiceProtocol {
  func fetchData(_ url: URL) async throws -> Data
}

struct APIService: APIServiceProtocol {
  func fetchData(_ url: URL) async throws -> Data {
    let (data,response) = try await URLSession.shared.data(from: url)
    guard let resp = response as? HTTPURLResponse,
          resp.statusCode == 200 else {
      throw APIError.networkError
    }
    return data
  }
}

enum APIError: Error {
  case decodingError
  case invalidURLError
  case networkError
}
