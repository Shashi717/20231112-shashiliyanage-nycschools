//
//  SATScoresViewModel.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import Foundation

protocol SATScoresViewModelDelegate: NSObject {
  // delegate to inform that view model has been updated
  func didUpdate(_ error: Error?)
}

class SATScoresViewModel {
  
  private let dbn: String
  private let apiClient: APIClient
  private let cache: LocalCache<SATScoresViewModel>
  private var satScores: SATScores?
  
  weak var delegate: SATScoresViewModelDelegate?
  
  var isSATScoresLoaded: Bool {
    return satScores != nil
  }
  var numOfTestTakers: String? {
    satScores?.numOfSATTestTakers
  }
  var criticalReadingAvgScore: String? {
    satScores?.satCriticalReadingAvgScore
  }
  var mathAvgScore: String? {
    satScores?.satMathAvgScore
  }
  var writingAvgScore: String? {
    satScores?.satWritingAvgScore
  }
  
  init(dbn: String, apiClient: APIClient, cache: LocalCache<SATScoresViewModel>) {
    self.dbn = dbn
    self.apiClient = apiClient
    self.cache = cache
  }
  
  func loadSATScores() async {
    guard let url = APIEndpoints().getSATScoresAPIEndpoint(for: dbn) else {
      // update the UI with error
      delegate?.didUpdate(APIError.invalidURLError)
      return
    }
    do {
      satScores = try await apiClient.fetchSATScores(url)
      cache.addItem(self, for: dbn)
      // call to update the UI as we've loaded the SAT scores
      delegate?.didUpdate(nil)
    } catch {
      // update the UI with error
      delegate?.didUpdate(error)
    }
  }
}
