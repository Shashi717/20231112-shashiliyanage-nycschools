//
//  SchoolsViewModel.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import Foundation

protocol SchoolsViewModelDelegate: NSObject {
  // delegate to inform that view model has been updated
  func didUpdate(_ error: Error?)
}

class SchoolsViewModel {
  private let apiClient: APIClient
  weak var delegate: SchoolsViewModelDelegate?

  var schools: [SchoolViewModel]?

  init(apiClient: APIClient) {
    self.apiClient = apiClient
  }

  func loadSchools() async {
    guard let url = APIEndpoints().getSchoolsAPIEndpoint() else {
      // update the UI with error
      delegate?.didUpdate(APIError.invalidURLError)
      return
    }
    do {
      let schools = try await apiClient.fetchSchools(url)
      self.schools = schools.compactMap { school in
        SchoolViewModel(school: school)
      }
      // call to update the UI as we've loaded the schools
      delegate?.didUpdate(nil)
    } catch {
      // update the UI with error
      delegate?.didUpdate(error)
    }
  }
}


