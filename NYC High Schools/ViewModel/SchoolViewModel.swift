//
//  SchoolViewModel.swift
//  NYC High Schools
//
//  Created by Shashi Liyanage on 11/8/23.
//

import Foundation

class SchoolViewModel {
  let school: School

  var name: String {
    school.schoolName
  }
  var location: String {
    school.location
  }
  var overview: String {
    school.overviewParagraph
  }
  var website: URL? {
    URL(string: school.website)
  }
  var phoneNumber: String {
    school.phoneNumber
  }

  init(school: School) {
    self.school = school
  }
}
