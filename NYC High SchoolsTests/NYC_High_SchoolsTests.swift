//
//  NYC_High_SchoolsTests.swift
//  NYC High SchoolsTests
//
//  Created by Shashi Liyanage on 11/8/23.
//

import XCTest
@testable import NYC_High_Schools

final class NYC_High_SchoolsTests: XCTestCase {

  var mockAPIClient: APIClient?

  override func setUpWithError() throws {
    // Put setup code here. This method is called before the invocation of each test method in the class.

    // setup a mockAPIClient with MockAPIService
    mockAPIClient = APIClient(apiService: MockAPIService())
  }

  override func tearDownWithError() throws {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    mockAPIClient = nil
  }

  func testCache() {
    let cache = LocalCache<NSString>(limit: 5)
    cache.addItem("1", for: "1")
    cache.addItem("2", for: "2")
    cache.addItem("3", for: "3")
    cache.addItem("4", for: "4")
    cache.addItem("5", for: "5")
    cache.addItem("6", for: "6")

    // Test that cache removes 1st item after adding the 6th
    XCTAssertNil(cache.getItem("1"))
    // Test that cache contains 2nd item
    XCTAssertTrue(cache.getItem("2")! == "2")
  }

  func testNYCHighSchoolJSON() async throws {
    guard let url = Bundle(for: NYC_High_SchoolsTests.self).url(forResource: "nyc_high_schools", withExtension: "json") else {
      XCTFail("Missing File")
      return
    }
    let schools = try await mockAPIClient?.fetchSchools(url)
    // test that schools are not nil
    XCTAssertNotNil(schools)
  }

  func testSAT08X282JSON() async throws {
    guard let url = Bundle(for: NYC_High_SchoolsTests.self).url(forResource: "sat_08X282", withExtension: "json") else {
      XCTFail("Missing File")
      return
    }
    let satScores = try await mockAPIClient?.fetchSATScores(url)
    // test that SAT scores are not nil
    XCTAssertNotNil(satScores)
  }
}

struct MockAPIService: APIServiceProtocol {
  func fetchData(_ url: URL) async throws -> Data {
    if let data = try? Data(contentsOf: url) {
      return data
    } else {
      throw APIError.networkError
    }
  }
}
